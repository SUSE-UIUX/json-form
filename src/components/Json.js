import React, { useContext, useEffect } from 'react'
import { StoreContextState } from '../utils/Store'
import Prism from 'prismjs'
import '../assets/scss/vendor/prism.css'

const Json = () => {
  const state = useContext(StoreContextState)
  useEffect(() => {
    Prism.highlightAll()
  })
  return (
    <div className='flex-content json'>
      <h2>Json output</h2>
      <pre>
        <code className='language-css'>
          {`{
  "id": "${state.id}",
  "name": "${state.name}",
  "img": "${state.img}",
  "role": "${state.role}",
  "location": "${state.location}",
  "quote": "${state.quote}",
},`}
        </code>
      </pre>
    </div>
  )
}

export default Json
